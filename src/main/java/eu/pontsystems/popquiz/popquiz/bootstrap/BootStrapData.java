package eu.pontsystems.popquiz.popquiz.bootstrap;

import eu.pontsystems.popquiz.popquiz.model.Valasz;
import eu.pontsystems.popquiz.popquiz.repository.EredmenyRepository;
import eu.pontsystems.popquiz.popquiz.repository.KerdesRepository;
import eu.pontsystems.popquiz.popquiz.repository.KerdoivRepository;
import eu.pontsystems.popquiz.popquiz.repository.ValaszRepository;
import eu.pontsystems.popquiz.popquiz.model.Kerdes;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class BootStrapData implements CommandLineRunner {

    private final EredmenyRepository eredmenyRepository;
    private final KerdesRepository kerdesRepository;
    private final ValaszRepository valaszRepository;
    private final KerdoivRepository kerdoivRepository;

    public BootStrapData(EredmenyRepository eredmenyRepository, KerdesRepository kerdesRepository, ValaszRepository valaszRepository, KerdoivRepository kerdoivRepository) {
        this.eredmenyRepository = eredmenyRepository;
        this.kerdesRepository = kerdesRepository;
        this.valaszRepository = valaszRepository;
        this.kerdoivRepository = kerdoivRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        Kerdes tmpKerdes = new Kerdes("Mi a neved?");
        List<Valasz> tmpValaszok = new ArrayList<>();
        tmpValaszok.add(new Valasz("A nevem Lancelot Degenere Camelot",true));
        tmpValaszok.add(new Valasz("Sir Robin, a cameloti.",true));
        tmpValaszok.add(new Valasz("Galahad, a cameloti!",true));
        tmpValaszok.add(new Valasz("Arthur vagyok, a britek királya.",true));
        tmpKerdes.setValaszok(tmpValaszok);
        kerdesRepository.save(tmpKerdes);
        for (Valasz valasz: tmpValaszok) {
            valasz.setKerdes(tmpKerdes);
            valaszRepository.save(valasz);
        }

        tmpKerdes = new Kerdes("Micsoda Sri Lanka fővárosa?");
        tmpValaszok = new ArrayList<>();
        tmpValaszok.add(new Valasz("Honnan tudjam én azt?!",true));
        tmpValaszok.add(new Valasz("Sri Jayawardenapura Kotte",false));
        tmpValaszok.add(new Valasz("Colombo",false));
        tmpKerdes.setValaszok(tmpValaszok);
        kerdesRepository.save(tmpKerdes);
        for (Valasz valasz: tmpValaszok) {
            valasz.setKerdes(tmpKerdes);
            valaszRepository.save(valasz);
        }

        tmpKerdes = new Kerdes("Mennyi egy töketlen fecske maximális repülési sebessége?");
        tmpValaszok = new ArrayList<>();
        tmpValaszok.add(new Valasz("Attól függ. Afrikai, vagy európai fecskéé?",true));
        tmpValaszok.add(new Valasz("1.2 Mach",false));
        tmpValaszok.add(new Valasz("140 km/h",false));
        tmpValaszok.add(new Valasz("Nem repül, hanem ugrál",false));
        tmpKerdes.setValaszok(tmpValaszok);
        kerdesRepository.save(tmpKerdes);
        for (Valasz valasz: tmpValaszok) {
            valasz.setKerdes(tmpKerdes);
            valaszRepository.save(valasz);
        }

        tmpKerdes = new Kerdes("És mi a kedvenc színed?");
        tmpValaszok = new ArrayList<>();
        tmpValaszok.add(new Valasz("Barna... nem, a kék!",true));
        tmpValaszok.add(new Valasz("Zöld",false));
        tmpKerdes.setValaszok(tmpValaszok);
        kerdesRepository.save(tmpKerdes);
        for (Valasz valasz: tmpValaszok) {
            valasz.setKerdes(tmpKerdes);
            valaszRepository.save(valasz);
        }

        tmpKerdes = new Kerdes("Honnan tudsz ennyit a töketlen fecskékről?");
        tmpValaszok = new ArrayList<>();
        tmpValaszok.add(new Valasz("Egy királynak értenie kell mindenhez.",true));
        tmpValaszok.add(new Valasz("A Gyalog galoppból!",false));
        tmpValaszok.add(new Valasz("Ornitológus vagyok.",false));
        tmpKerdes.setValaszok(tmpValaszok);
        kerdesRepository.save(tmpKerdes);
        for (Valasz valasz: tmpValaszok) {
            valasz.setKerdes(tmpKerdes);
            valaszRepository.save(valasz);
        }

        tmpKerdes = new Kerdes("Piros dombon harminc fehér ló abrakol, dobrokol, majd rajta a béklyó?");
        tmpValaszok = new ArrayList<>();
        tmpValaszok.add(new Valasz("A fogak. (de minekünk cak hat van)",true));
        tmpValaszok.add(new Valasz("A Fehér ló fia",false));
        tmpKerdes.setValaszok(tmpValaszok);
        kerdesRepository.save(tmpKerdes);
        for (Valasz valasz: tmpValaszok) {
            valasz.setKerdes(tmpKerdes);
            valaszRepository.save(valasz);
        }

        tmpKerdes = new Kerdes("Hang nélkül kiált, szárnytalan lebeg, fog nélkül kirág, szájtalan hebeg?");
        tmpValaszok = new ArrayList<>();
        tmpValaszok.add(new Valasz("A szél",true));
        tmpValaszok.add(new Valasz("Kísértet",false));
        tmpValaszok.add(new Valasz("Néma kísértet",false));
        tmpKerdes.setValaszok(tmpValaszok);
        kerdesRepository.save(tmpKerdes);
        for (Valasz valasz: tmpValaszok) {
            valasz.setKerdes(tmpKerdes);
            valaszRepository.save(valasz);
        }

        tmpKerdes = new Kerdes("Kék arc szeme, szemben zöld arc szeme, \"Az a szem, mint ez a szem\", Így az első kényesen, \"De odalenn, Nem idefenn\"?");
        tmpValaszok = new ArrayList<>();
        tmpValaszok.add(new Valasz("A nap, amint lenéz a százszorszépekre.",true));
        tmpValaszok.add(new Valasz("Teletubies",false));
        tmpKerdes.setValaszok(tmpValaszok);
        kerdesRepository.save(tmpKerdes);
        for (Valasz valasz: tmpValaszok) {
            valasz.setKerdes(tmpKerdes);
            valaszRepository.save(valasz);
        }

        tmpKerdes = new Kerdes("Nem láthatni, nem tapinthatni, nem hallhatni, nem szagolhatni. Túl csillagokon, dombok tövében kitölti az űrt egészen, sereghajtó, bár járt legelöl, életet végez, kacajt megöl?");
        tmpValaszok = new ArrayList<>();
        tmpValaszok.add(new Valasz("A sötétség!",true));
        tmpValaszok.add(new Valasz("Levegő",false));
        tmpValaszok.add(new Valasz("Nyelem",false));
        tmpKerdes.setValaszok(tmpValaszok);
        kerdesRepository.save(tmpKerdes);
        for (Valasz valasz: tmpValaszok) {
            valasz.setKerdes(tmpKerdes);
            valaszRepository.save(valasz);
        }

        tmpKerdes = new Kerdes("Pánt, kulcs, fedél nélküli doboz, de a kincse benn aranyos?");
        tmpValaszok = new ArrayList<>();
        tmpValaszok.add(new Valasz("Tojásszok!",true));
        tmpValaszok.add(new Valasz("Pánt, kulcs, fedél nélküli doboz.",false));
        tmpValaszok.add(new Valasz("A kalózok kincse",false));
        tmpKerdes.setValaszok(tmpValaszok);
        kerdesRepository.save(tmpKerdes);
        for (Valasz valasz: tmpValaszok) {
            valasz.setKerdes(tmpKerdes);
            valaszRepository.save(valasz);
        }

        tmpKerdes = new Kerdes("Él lélegzettelen, halotti hidegen; Sose szomjas, kortya örök, páncélt hord, mi sose zörög?");
        tmpValaszok = new ArrayList<>();
        tmpValaszok.add(new Valasz("Hal, hal!",true));
        tmpValaszok.add(new Valasz("Nem hall, nem hall!",false));
        tmpValaszok.add(new Valasz("Nazgûl",false));
        tmpKerdes.setValaszok(tmpValaszok);
        kerdesRepository.save(tmpKerdes);
        for (Valasz valasz: tmpValaszok) {
            valasz.setKerdes(tmpKerdes);
            valaszRepository.save(valasz);
        }

        tmpKerdes = new Kerdes("Lábanincs feküdt egylábon, kétláb a közelben ült háromlábon, négyláb is kapott?");
        tmpValaszok = new ArrayList<>();
        tmpValaszok.add(new Valasz("Hal egy kisasztalon, ember ül az asztalnál háromlábú széken, és a kutyának jut a csontja.",true));
        tmpValaszok.add(new Valasz("Százlábú",false));
        tmpKerdes.setValaszok(tmpValaszok);
        kerdesRepository.save(tmpKerdes);
        for (Valasz valasz: tmpValaszok) {
            valasz.setKerdes(tmpKerdes);
            valaszRepository.save(valasz);
        }

        tmpKerdes = new Kerdes("Mindent befal, pofája tátott: Szárnyast, szárnyatlant, fát, virágot, vasat csócsál, acélt ropogtat, fogával követ is kikoptat; Várost dönt, elveszejt királyt, sok magas hegyet földbe vájt?");
        tmpValaszok = new ArrayList<>();
        tmpValaszok.add(new Valasz("Idő! Idő!",true));
        tmpValaszok.add(new Valasz("Smaug, a sárkány",false));
        tmpKerdes.setValaszok(tmpValaszok);
        kerdesRepository.save(tmpKerdes);
        for (Valasz valasz: tmpValaszok) {
            valasz.setKerdes(tmpKerdes);
            valaszRepository.save(valasz);
        }

        tmpKerdes = new Kerdes("Mi van a zsebemben?");
        tmpValaszok = new ArrayList<>();
        tmpValaszok.add(new Valasz("Ez nem tisztesszégesz! Nem tisztesszéges!",true));
        tmpValaszok.add(new Valasz("Egy gyűrű!",false));
        tmpValaszok.add(new Valasz("S&W .357 Magnum",false));
        tmpKerdes.setValaszok(tmpValaszok);
        kerdesRepository.save(tmpKerdes);
        for (Valasz valasz: tmpValaszok) {
            valasz.setKerdes(tmpKerdes);
            valaszRepository.save(valasz);
        }

/*
        Kerdoiv tmpKerdoiv = new Kerdoiv("Tibi");
        List<Eredmeny> tmpEredmenyek = new ArrayList<>();
        Optional<Kerdes> eKerdes1 = kerdesRepository.findById(1L);
        Optional<Valasz> eValasz1 = valaszRepository.findById(2L);
        tmpEredmenyek.add(new Eredmeny(LocalDateTime.now(),eKerdes1.get(),eValasz1.get()));
        Optional<Kerdes> eKerdes2 = kerdesRepository.findById(5L);
        Optional<Valasz> eValasz2 = valaszRepository.findById(7L);
        tmpEredmenyek.add(new Eredmeny(LocalDateTime.now(),eKerdes2.get(),eValasz2.get()));
        Optional<Kerdes> eKerdes3 = kerdesRepository.findById(10L);
        Optional<Valasz> eValasz3 = valaszRepository.findById(11L);
        tmpEredmenyek.add(new Eredmeny(LocalDateTime.now(),eKerdes3.get(),eValasz3.get()));
        tmpKerdoiv.setHelyesValaszok(2);
        tmpKerdoiv.setEredmenyek(tmpEredmenyek);
        kerdoivRepository.save(tmpKerdoiv);
        for (Eredmeny eredmeny: tmpEredmenyek) {
            eredmeny.setKerdoiv(tmpKerdoiv);
            eredmenyRepository.save(eredmeny);
        }
*/
        System.out.println("Bootstrap sout...");
        System.out.println("Kérdések száma:" + kerdesRepository.count());
        System.out.println("Válaszok száma:" + valaszRepository.count());

    }
}
