package eu.pontsystems.popquiz.popquiz.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class nemTalalomException extends Exception {

    private static final long serialVersionUID = 1L;

    public nemTalalomException(String message) {
        super(message);
    }
}
