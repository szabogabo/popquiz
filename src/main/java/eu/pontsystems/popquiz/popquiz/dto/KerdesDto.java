package eu.pontsystems.popquiz.popquiz.dto;

import lombok.Data;

@Data
public class KerdesDto {

    private Long id;
    private String kerdesSzoveg;

}
