package eu.pontsystems.popquiz.popquiz.service;

import eu.pontsystems.popquiz.popquiz.dto.KerdesDto;
import eu.pontsystems.popquiz.popquiz.exception.nemTalalomException;
import eu.pontsystems.popquiz.popquiz.model.Kerdes;
import eu.pontsystems.popquiz.popquiz.model.Kerdoiv;

import java.util.List;

public interface KerdesService {

    KerdesDto getKerdesById(Long kerdesId) throws nemTalalomException;

    List<KerdesDto> getAllKerdes();

    KerdesDto saveKerdes(KerdesDto kerdesDto) throws nemTalalomException;

    KerdesDto saveKerdesById(Long kerdesId, KerdesDto kerdesDto) throws nemTalalomException;

    Boolean hasNev(String nev);

    Kerdoiv kerdoivSetup(String nev);

    void saveEredmeny(Kerdoiv kerdoiv, String kerdesId, String valaszId);

    List<Kerdes> getAvailableKerdes(String nev);

    Kerdes getOneKerdes(List<Kerdes> kerdesek);

}
