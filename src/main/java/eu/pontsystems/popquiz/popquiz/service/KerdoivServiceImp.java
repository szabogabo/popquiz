package eu.pontsystems.popquiz.popquiz.service;

import eu.pontsystems.popquiz.popquiz.model.Eredmeny;
import eu.pontsystems.popquiz.popquiz.model.Kerdoiv;
import eu.pontsystems.popquiz.popquiz.model.Valasz;
import eu.pontsystems.popquiz.popquiz.repository.KerdoivRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class KerdoivServiceImp implements KerdoivService {

    private final KerdoivRepository kerdoivRepository;

    public KerdoivServiceImp(KerdoivRepository kerdoivRepository) {
        this.kerdoivRepository = kerdoivRepository;
    }

    public List<Valasz> getAllHelyesValasz(Kerdoiv kerdoiv) {
        List<Eredmeny> eredmenyek = kerdoiv.getEredmenyek();
        List<Valasz> helyesValaszok = new ArrayList<>();
        for (Eredmeny eredmeny: eredmenyek) {
            if (eredmeny.getValasz().isHelyes()) {
                helyesValaszok.add(eredmeny.getValasz());
            }
        }
        return helyesValaszok;
    }

    public void updateHelyesValaszok(Kerdoiv kerdoiv) {
        List<Valasz> helyesValaszok = getAllHelyesValasz(kerdoiv);
        kerdoiv.setHelyesValaszok(helyesValaszok.size());
    }

    public List<Kerdoiv> getAllKerdoiv(){
        return kerdoivRepository.findAllByOrderByHelyesValaszokDesc();
    }
}
