package eu.pontsystems.popquiz.popquiz.service;

import eu.pontsystems.popquiz.popquiz.model.Kerdoiv;
import eu.pontsystems.popquiz.popquiz.model.Valasz;

import java.util.List;

public interface KerdoivService {

    List<Valasz> getAllHelyesValasz(Kerdoiv kerdoiv);

    void updateHelyesValaszok(Kerdoiv kerdoiv);

    List<Kerdoiv> getAllKerdoiv();

}
