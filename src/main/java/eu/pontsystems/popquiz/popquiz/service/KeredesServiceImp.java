package eu.pontsystems.popquiz.popquiz.service;

import eu.pontsystems.popquiz.popquiz.dto.KerdesDto;
import eu.pontsystems.popquiz.popquiz.exception.nemTalalomException;
import eu.pontsystems.popquiz.popquiz.mapper.KerdesMapper;
import eu.pontsystems.popquiz.popquiz.model.Eredmeny;
import eu.pontsystems.popquiz.popquiz.model.Kerdes;
import eu.pontsystems.popquiz.popquiz.model.Kerdoiv;
import eu.pontsystems.popquiz.popquiz.repository.EredmenyRepository;
import eu.pontsystems.popquiz.popquiz.repository.KerdesRepository;
import eu.pontsystems.popquiz.popquiz.repository.KerdoivRepository;
import eu.pontsystems.popquiz.popquiz.repository.ValaszRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
@AllArgsConstructor
public class KeredesServiceImp implements KerdesService {

    private final KerdoivRepository kerdoivRepository;
    private final EredmenyRepository eredmenyRepository;
    private final KerdesRepository kerdesRepository;
    private final ValaszRepository valaszRepository;
    private final KerdesMapper kerdesMapper;

    //Kérdés visszaadása ID alapján
    public KerdesDto getKerdesById(Long kerdesId) throws nemTalalomException {
        return kerdesMapper.entityToDto(kerdesRepository.findById(kerdesId).orElseThrow(()-> new nemTalalomException("Nincs ilyen kérdés! Id:"+kerdesId)));
    }

    //Összes kérdés visszaadása
    public List<KerdesDto> getAllKerdes(){
        return kerdesMapper.entityToDto(kerdesRepository.findAll());
    }

    //Új kérdés mentése
    public KerdesDto saveKerdes(KerdesDto kerdesDto) throws nemTalalomException {
        Kerdes kerdes = kerdesMapper.dtoToEntity(kerdesDto);
        kerdesRepository.save(kerdes);
        return getKerdesById(kerdes.getId());
    }

    //Kérdés módosítás
    public KerdesDto saveKerdesById(Long kerdesId, KerdesDto kerdesDto) throws nemTalalomException {
        kerdesRepository.findById(kerdesId).orElseThrow(()-> new nemTalalomException("Nincs ilyen kérdés! Id:"+kerdesId));
        Kerdes kerdes = kerdesMapper.dtoToEntity(kerdesDto);
        kerdes.setId(kerdesId);
        kerdesRepository.save(kerdes);
        return getKerdesById(kerdes.getId());
    }

    //Név ellenőrzés
    public Boolean hasNev(String nev){
        return !nev.isEmpty();
    }

    //Beállítja a kérddőív osztályt
    public Kerdoiv kerdoivSetup(String nev){
        Optional<Kerdoiv> dbKerdoiv = kerdoivRepository.findByTulajdonos(nev);
        Kerdoiv aktKerdoiv = new Kerdoiv();

        if (dbKerdoiv.isPresent()) {
            aktKerdoiv = dbKerdoiv.get();
        } else {
            aktKerdoiv.setTulajdonos(nev);
            kerdoivRepository.save(aktKerdoiv);
        }

        return aktKerdoiv;
    }

    public void saveEredmeny(Kerdoiv aktKerdoiv, String kerdesId, String valaszId) {
        if (kerdesId != null && valaszId != null) {
            Long kerdesIdLong = Long.parseLong(kerdesId);
            Long valaszIdLong = Long.parseLong(valaszId);

            Eredmeny aktEredmeny = new Eredmeny();
            aktEredmeny.setKitoltve(LocalDateTime.now());
            aktEredmeny.setKerdoiv(aktKerdoiv);
            aktEredmeny.setKerdes(kerdesRepository.findById(kerdesIdLong).get());
            aktEredmeny.setValasz(valaszRepository.findById(valaszIdLong).get());

            eredmenyRepository.save(aktEredmeny);
        }
    }

    public List<Kerdes> getAvailableKerdes(String nev) {
        List<Kerdes> osszesKerdes = kerdesRepository.findAll();
        List<Kerdes> kitoltottKerdes = kerdesRepository.findAllByEredmenyekKerdoivTulajdonos(nev);
        osszesKerdes.removeAll(kitoltottKerdes);

        return osszesKerdes;
    }

    public Kerdes getOneKerdes(List<Kerdes> kerdesek) {

        Random rand = new Random();
        int rndIdx = rand.nextInt(kerdesek.size());
        Kerdes nextKerdes = kerdesek.get(rndIdx);
        //Kerdes nextKerdes = kerdesek.get(0);

        return nextKerdes;
    };

}
