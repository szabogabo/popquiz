package eu.pontsystems.popquiz.popquiz.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter @Setter @NoArgsConstructor
public class Kerdoiv {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String tulajdonos;

    private Integer helyesValaszok;

    @OneToMany(mappedBy = "kerdoiv")
    private List<Eredmeny> eredmenyek = new ArrayList<>();

    public Kerdoiv(String tulajdonos) {
        this.tulajdonos = tulajdonos;
    }

    @Override
    public String toString() {
        return "Kerdoiv{" +
                "id=" + id +
                ", tulajdonos='" + tulajdonos + '\'' +
                ", helyesValaszok=" + helyesValaszok +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Kerdoiv kerdoiv = (Kerdoiv) o;

        return id != null ? id.equals(kerdoiv.id) : kerdoiv.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
