package eu.pontsystems.popquiz.popquiz.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter @Setter @NoArgsConstructor @Data
public class Valasz {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String valaszSzoveg;
    private boolean helyes;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "kerdes_id", referencedColumnName = "id")
    private Kerdes kerdes;

    @JsonIgnore
    @OneToMany(mappedBy = "valasz")
    private List<Eredmeny> eredmenyek = new ArrayList<>();

    public Valasz(String valaszSzoveg, boolean helyes) {
        this.valaszSzoveg = valaszSzoveg;
        this.helyes = helyes;
    }

    @Override
    public String toString() {
        return "Valasz{" +
                "id=" + id +
                ", valaszSzoveg='" + valaszSzoveg + '\'' +
                ", helyes=" + helyes +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        //if (this.id == null) return false;
        if (o == null || getClass() != o.getClass()) return false;

        Valasz valasz = (Valasz) o;

        return id == valasz.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
