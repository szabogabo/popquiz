package eu.pontsystems.popquiz.popquiz.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter @Setter @NoArgsConstructor @ToString
public class Kerdes {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String kerdesSzoveg;

    @OneToMany(mappedBy = "kerdes")
    private List<Valasz> valaszok = new ArrayList<>();

    @OneToMany(mappedBy = "kerdes")
    private List<Eredmeny> eredmenyek = new ArrayList<>();

    public Kerdes(String kerdesSzoveg) {
        this.kerdesSzoveg = kerdesSzoveg;
    }

}
