package eu.pontsystems.popquiz.popquiz.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter @Setter @NoArgsConstructor
public class Eredmeny {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private LocalDateTime kitoltve;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "kerdoiv_id", referencedColumnName = "id")
    private Kerdoiv kerdoiv;

    @ManyToOne
    @JoinColumn(name = "kerdes_id", referencedColumnName = "id")
    private Kerdes kerdes;

    @ManyToOne
    @JoinColumn(name = "valasz_id", referencedColumnName = "id")
    private Valasz valasz;

    public Eredmeny(LocalDateTime kitoltve, Kerdes kerdes, Valasz valasz) {
        this.kitoltve = kitoltve;
        this.kerdes = kerdes;
        this.valasz = valasz;
    }

    @Override
    public String toString() {
        return "Eredmeny{" +
                "id=" + id +
                ", kitoltve=" + kitoltve +
                ", kerdes=" + kerdes.toString() +
                ", valasz=" + valasz.toString() +

                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Eredmeny eredmeny = (Eredmeny) o;

        return id == eredmeny.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
