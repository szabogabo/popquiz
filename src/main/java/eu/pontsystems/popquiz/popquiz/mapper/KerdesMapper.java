package eu.pontsystems.popquiz.popquiz.mapper;

import eu.pontsystems.popquiz.popquiz.dto.KerdesDto;
import eu.pontsystems.popquiz.popquiz.model.Kerdes;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class KerdesMapper {

    public KerdesDto entityToDto(Kerdes kerdes) {

        KerdesDto dto = new KerdesDto();
        dto.setId(kerdes.getId());
        dto.setKerdesSzoveg(kerdes.getKerdesSzoveg());

        return dto;
    }

    public List<KerdesDto> entityToDto(List<Kerdes> kerdesek) {
        return kerdesek.stream().map(this::entityToDto).collect(Collectors.toList());
    }

    public Kerdes dtoToEntity(KerdesDto dto){

        Kerdes kerdes = new Kerdes();

        kerdes.setKerdesSzoveg(dto.getKerdesSzoveg());

        return kerdes;
    }

}
