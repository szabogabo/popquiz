package eu.pontsystems.popquiz.popquiz.repository;

import eu.pontsystems.popquiz.popquiz.model.Valasz;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ValaszRepository extends JpaRepository<Valasz, Long> {

    //Visszaadja a felhasználó helyes válaszait
    //@Query("select v from Valasz v left join Eredmeny e on v = e.valasz left join Kerdoiv k on k = e.kerdoiv where v.helyes = true and k.tulajdonos = ?1")
    List<Valasz> findAllByHelyesTrueAndEredmenyekKerdoivTulajdonos(String tulajdonos);

}
