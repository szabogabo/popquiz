package eu.pontsystems.popquiz.popquiz.repository;

import eu.pontsystems.popquiz.popquiz.model.Kerdoiv;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface KerdoivRepository extends JpaRepository<Kerdoiv, Long> {

    Optional<Kerdoiv> findByTulajdonos(String tulajdonos);

    //@Query("select k from #{#entityName} k order by k.helyesValaszok desc")
    List<Kerdoiv> findAllByOrderByHelyesValaszokDesc();

}
