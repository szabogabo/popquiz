package eu.pontsystems.popquiz.popquiz.repository;

import eu.pontsystems.popquiz.popquiz.model.Kerdes;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface KerdesRepository extends JpaRepository<Kerdes, Long> {

    //Visszaadja az összes olyan kérdést, amit az adott felhasználó válaszolt meg
    //@Query("select k from Kerdes k left join Eredmeny e on k = e.kerdes left join Kerdoiv ki on e.kerdoiv = ki where ki.tulajdonos = ?1")
    List<Kerdes> findAllByEredmenyekKerdoivTulajdonos(String tulajdonos);

}
