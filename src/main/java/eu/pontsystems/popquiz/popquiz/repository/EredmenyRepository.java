package eu.pontsystems.popquiz.popquiz.repository;

import eu.pontsystems.popquiz.popquiz.model.Eredmeny;
import org.springframework.data.repository.CrudRepository;

public interface EredmenyRepository extends CrudRepository<Eredmeny, Long> {
}
