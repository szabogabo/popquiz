package eu.pontsystems.popquiz.popquiz.controller;

import eu.pontsystems.popquiz.popquiz.dto.KerdesDto;
import eu.pontsystems.popquiz.popquiz.exception.nemTalalomException;
import eu.pontsystems.popquiz.popquiz.repository.KerdesRepository;
import eu.pontsystems.popquiz.popquiz.service.KerdesService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class KerdesRestController {

    private final KerdesRepository kerdesRepository;
    private final KerdesService kerdesService;

    //Összes kérdés lekérdezése
    @GetMapping("/kerdesek")
    public ResponseEntity<List<KerdesDto>> osszesKerdes(){
        return ResponseEntity.ok().body(kerdesService.getAllKerdes());
    }

    //Kérdés visszadása ID alapján
    @GetMapping("/kerdes/{id}")
    public ResponseEntity<KerdesDto> getKerdes(@PathVariable(name = "id") Long kerdesId) throws nemTalalomException {
        KerdesDto kerdesDto = kerdesService.getKerdesById(kerdesId);
        return ResponseEntity.ok().body(kerdesDto);
    }

    //Új kérdés
    @PostMapping("/kerdes")
    public ResponseEntity<KerdesDto> ujKerdes(@Valid @RequestBody KerdesDto kerdesDto) throws nemTalalomException {
        return ResponseEntity.ok().body(kerdesService.saveKerdes(kerdesDto));
    }

    //Kérdés szöveg módosítás ID alapján
    @PutMapping("/kerdes/{id}")
    public ResponseEntity<KerdesDto> modKerdes(@PathVariable(name = "id") Long kerdesId, @Valid @RequestBody KerdesDto kerdesDto) throws nemTalalomException {
        return ResponseEntity.ok().body(kerdesService.saveKerdesById(kerdesId,kerdesDto));
    }

}
