package eu.pontsystems.popquiz.popquiz.controller;

import eu.pontsystems.popquiz.popquiz.model.Kerdoiv;
import eu.pontsystems.popquiz.popquiz.exception.nemTalalomException;
import eu.pontsystems.popquiz.popquiz.repository.KerdoivRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class KerdoivRestController {

    private final KerdoivRepository kerdoivRepository;

    public KerdoivRestController(KerdoivRepository kerdoivRepository) {
        this.kerdoivRepository = kerdoivRepository;
    }

    //Összes kérdőív lekérdezése
    @GetMapping("/kerdoivek")
    public List<Kerdoiv> getAllKerdoiv(){
        return kerdoivRepository.findAll();
    }

    //Kérdőív visszaadása id alapján
    @GetMapping("/kerdoiv/{id}")
    public ResponseEntity<Kerdoiv> getKerdoivById(@PathVariable(name = "id") Long kerdoivId) throws nemTalalomException {
        Kerdoiv kerdoiv = kerdoivRepository.findById(kerdoivId).orElseThrow(() -> new nemTalalomException("Nincs ilyen kérdőív! ID: "+kerdoivId));
        return ResponseEntity.ok().body(kerdoiv);
    }

}
