package eu.pontsystems.popquiz.popquiz.controller;

import eu.pontsystems.popquiz.popquiz.model.Eredmeny;
import eu.pontsystems.popquiz.popquiz.model.Valasz;
import eu.pontsystems.popquiz.popquiz.exception.nemTalalomException;
import eu.pontsystems.popquiz.popquiz.repository.EredmenyRepository;
import eu.pontsystems.popquiz.popquiz.repository.ValaszRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class EredmenyRestController {

    private final EredmenyRepository eredmenyRepository;
    private final ValaszRepository valaszRepository;

    public EredmenyRestController(EredmenyRepository eredmenyRepository, ValaszRepository valaszRepository) {
        this.eredmenyRepository = eredmenyRepository;
        this.valaszRepository = valaszRepository;
    }

    //Összes eredmény lekérdezése
    @GetMapping("/eredmenyek")
    public Iterable<Eredmeny> osszesEredmeny() { return eredmenyRepository.findAll(); }

    //Eredmény lekérdezése ID alapján
    @GetMapping("/eredmeny/{id}")
    public ResponseEntity<Eredmeny> getEredmenyById(@PathVariable(name = "id") Long eredmenyId) throws nemTalalomException {
        Eredmeny eredmeny = eredmenyRepository.findById(eredmenyId).orElseThrow(() -> new nemTalalomException("Nincs ilyen eredmény! ID:"+eredmenyId));
        return ResponseEntity.ok().body(eredmeny);
    }

    //Eredmény válasz módosítása ID alapján
    @PutMapping("/eredmenymod/{id}")
    public ResponseEntity<Eredmeny> modEredmenyById(@PathVariable(name = "id") Long eredmenyId, @RequestBody Eredmeny bejovoEredmeny) throws nemTalalomException {
        Eredmeny eredmeny = eredmenyRepository.findById(eredmenyId).orElseThrow(() -> new nemTalalomException("Nincs ilyen eredmény! ID:"+eredmenyId));
        Long valaszId = bejovoEredmeny.getValasz().getId();
        Valasz valasz = valaszRepository.findById(valaszId).orElseThrow(() -> new nemTalalomException("Hibás eredményválasz! ID:"+valaszId));
        eredmeny.setValasz(valasz);
        eredmenyRepository.save(eredmeny);
        return ResponseEntity.ok().body(eredmeny);
    }

    //Eredmény törlése
    @DeleteMapping("/eredmenytorol/{id}")
    public ResponseEntity delEredmenyById(@PathVariable(name = "id") Long eredmenyId) throws nemTalalomException {
        eredmenyRepository.findById(eredmenyId).orElseThrow(() -> new nemTalalomException("Nincs ilyen eredmény! ID:"+eredmenyId));
        eredmenyRepository.deleteById(eredmenyId);
        return ResponseEntity.ok().build();
    }
}
