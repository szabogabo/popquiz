package eu.pontsystems.popquiz.popquiz.controller;

import eu.pontsystems.popquiz.popquiz.model.Kerdes;
import eu.pontsystems.popquiz.popquiz.model.Kerdoiv;
import eu.pontsystems.popquiz.popquiz.repository.KerdesRepository;
import eu.pontsystems.popquiz.popquiz.repository.KerdoivRepository;
import eu.pontsystems.popquiz.popquiz.service.KerdesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("kerdesek")
public class KeredesController {

    private final KerdesRepository kerdesRepository;
    private final KerdoivRepository kerdoivRepository;
    private final KerdesService kerdesService;

    @Autowired
    public KeredesController(
            KerdesRepository kerdesRepository,
            KerdoivRepository kerdoivRepository,
            KerdesService kerdesService) {
        this.kerdesRepository = kerdesRepository;
        this.kerdoivRepository = kerdoivRepository;
        this.kerdesService = kerdesService;
    }

    @RequestMapping("getlist")
    public String osszesKerdes(Model model) {

        model.addAttribute("kerdes",new Kerdes());
        if (kerdesRepository.count() == 0) {
            model.addAttribute("kerdesek", "NA");
        }
        else {
            model.addAttribute("kerdesek", kerdesRepository.findAll());
        }

        return "kerdesek";
    }

    @PostMapping("ujkerdes")
    public String ujKerdes(@Valid Kerdes kerdes, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "ujkeredes";
        }

        kerdesRepository.save(kerdes);
        return "redirect:getlist";
    }

    @PostMapping("random_kerdes")
    public String randomKerdes(@RequestParam("nev") String nev, @RequestParam(value = "kerdes_id", required = false) String kerdesId, @RequestParam(value = "valasz_id", required = false) String valaszId,  Model model) {

        //hasNev - névellenőrzés
        if (!kerdesService.hasNev(nev)) {
            model.addAttribute("errNincsNev","hiba");
            model.addAttribute("kerdoivek", kerdoivRepository.findAll());
            return "index";
        }

        //kerdoivSetup
        Kerdoiv aktKerdoiv = kerdesService.kerdoivSetup(nev);

        //saveEredmény
        kerdesService.saveEredmeny(aktKerdoiv, kerdesId, valaszId);

        //Még elérhető kérdések lekérdezése
        List<Kerdes> elerhetoKerdesek = kerdesService.getAvailableKerdes(nev);

        if (elerhetoKerdesek.size() == 0) {
            model.addAttribute("nev", nev);
            model.addAttribute("kerdes", "NA");
            model.addAttribute("valaszok", "NA");
        } else {
            Kerdes nextKerdes = kerdesService.getOneKerdes(elerhetoKerdesek);
            model.addAttribute("nev", nev);
            model.addAttribute("kerdes", nextKerdes);
            model.addAttribute("valaszok", nextKerdes.getValaszok());
        }

        return "random_kerdes";
    }

}
