package eu.pontsystems.popquiz.popquiz.controller;

import eu.pontsystems.popquiz.popquiz.model.Kerdes;
import eu.pontsystems.popquiz.popquiz.model.Valasz;
import eu.pontsystems.popquiz.popquiz.repository.KerdesRepository;
import eu.pontsystems.popquiz.popquiz.repository.ValaszRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequestMapping("valaszok")
public class ValaszController {

    private final ValaszRepository valaszRepository;
    private final KerdesRepository kerdesRepository;

    public ValaszController(ValaszRepository valaszRepository, KerdesRepository kerdesRepository) {
        this.valaszRepository = valaszRepository;
        this.kerdesRepository = kerdesRepository;
    }

    @RequestMapping("getlist")
    public String showFirst(@RequestParam(value = "kerdes_id", required = false) Long id, Model model) {

        //Ez csúnya! Javítsd!
        if(id == null){
            id = 1L;
        }

        model.addAttribute("kerdesek", kerdesRepository.findAll());

        Optional<Kerdes> akt_kerdes = kerdesRepository.findById(id);
        model.addAttribute("akt_kerdes_id", akt_kerdes.get().getId());
        model.addAttribute("valaszok", akt_kerdes.get().getValaszok());

        return "valaszok";
    }

    @RequestMapping("ujvalasz")
    public String ujValasz(@RequestParam(name="kerdes_id") Long kerdesId, @RequestParam(name="valasz_szoveg") String valaszSzoveg, @RequestParam(name="helyes") Boolean helyes, Model model) {

        Optional<Kerdes> akt_kerdes = kerdesRepository.findById(kerdesId);
        Valasz akt_valasz = new Valasz(valaszSzoveg, helyes);
        akt_valasz.setKerdes(akt_kerdes.get());

        valaszRepository.save(akt_valasz);

        model.addAttribute("kerdesek", kerdesRepository.findAll());
        model.addAttribute("akt_kerdes_id", kerdesId);
        model.addAttribute("valaszok", akt_kerdes.get().getValaszok());

        return "valaszok";
    }


}
