package eu.pontsystems.popquiz.popquiz.controller;

import eu.pontsystems.popquiz.popquiz.model.Valasz;
import eu.pontsystems.popquiz.popquiz.exception.nemTalalomException;
import eu.pontsystems.popquiz.popquiz.repository.ValaszRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class ValaszRestController {

    private final ValaszRepository valaszRepository;

    public ValaszRestController(ValaszRepository valaszRepository) {
        this.valaszRepository = valaszRepository;
    }

    //Válasz lekérdezés ID alapján
    @GetMapping("/valasz/{id}")
    public ResponseEntity<Valasz> getValasz(@PathVariable(name = "id") Long valaszId) throws nemTalalomException {
        Valasz valasz = valaszRepository.findById(valaszId).orElseThrow(() -> new nemTalalomException("Nincs ilyen válasz! ID:"+valaszId));
        return ResponseEntity.ok().body(valasz);
    }

    //Válasz módosítása ID alapján
    @PutMapping("/valaszmod/{id}")
    public ResponseEntity<Valasz> getValasz(@PathVariable(name = "id") Long valaszId, @RequestBody Valasz bejovoValasz) throws nemTalalomException {
        Valasz valasz = valaszRepository.findById(valaszId).orElseThrow(() -> new nemTalalomException("Nincs ilyen válasz! ID:" + valaszId));
        valasz.setKerdes(bejovoValasz.getKerdes());
        valasz.setValaszSzoveg(bejovoValasz.getValaszSzoveg());
        valasz.setHelyes(bejovoValasz.isHelyes());
        valaszRepository.save(valasz);
        return ResponseEntity.ok().body(valasz);
    }

}
