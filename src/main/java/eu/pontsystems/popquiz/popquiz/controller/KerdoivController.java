package eu.pontsystems.popquiz.popquiz.controller;

import eu.pontsystems.popquiz.popquiz.model.Kerdoiv;
import eu.pontsystems.popquiz.popquiz.repository.KerdoivRepository;
import eu.pontsystems.popquiz.popquiz.service.KerdoivService;
import eu.pontsystems.popquiz.popquiz.service.KerdoivServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class KerdoivController {

    private final KerdoivRepository kerdoivRepository;

    @Autowired
    private final KerdoivService kerdoivService;

    public KerdoivController(KerdoivRepository kerdoivRepository, KerdoivServiceImp kerdoivServices) {
        this.kerdoivRepository = kerdoivRepository;
        this.kerdoivService = kerdoivServices;
    }

    @RequestMapping("/")
    public String getList(Model model) {
        if (kerdoivRepository.count() == 0) {
            model.addAttribute("kerdoivek", "NA");
        }
        else {
            List<Kerdoiv> kerdoivek = kerdoivRepository.findAll();
            for (Kerdoiv kerdoiv: kerdoivek) {
                kerdoivService.updateHelyesValaszok(kerdoiv);
                kerdoivRepository.save(kerdoiv);
            }
            model.addAttribute("kerdoivek", kerdoivService.getAllKerdoiv());
        }
        return "index";
    }

}
