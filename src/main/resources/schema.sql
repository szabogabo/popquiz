-- we don't know how to generate database DB (class Database) :(
create table KERDES
(
    ID BIGINT auto_increment
        primary key,
    KERDESSZOVEG VARCHAR2(255),
    HIBA BOOL
);

create table VALASZ
(
    ID BIGINT auto_increment
        primary key,
    KERDES_ID BIGINT
        references KERDES (ID),
    VALASZSZOVEG VARCHAR2(255),
    HELYES BOOL
);

create table EREDMENY
(
    ID BIGINT auto_increment
        primary key,
    KERDES_ID BIGINT
        references KERDES (ID),
    VALASZ_ID BIGINT
        references VALASZ (ID),
    FELHASZNALO VARCHAR2(255),
    KITOLTVE DATETIME
);

