# Popkultúrális QUIZ #

Igazából ez csak egy teszt halmaz, ami a Spring-Boot tanulmányaimat rögzíti.

### Az alábbi dolgokat néztem át ###

* IntelliJ-be épített Spring Initializr-el létrehoztam egy MAVEN projektet
* Dependenciák: Spring Boot Devtools, Lombok, Spring Web, Thymeleaf, Spring Data, JPA, H2 Database, Spring Boot Actuator
* A fentieket felhasználva létrehoztam egy CRUD alkalmazást felülettel
* REST API-kat fejlesztettem (tesztelés Postman-el)
* Git (Bitbucket és Sourcetree)

### A program lényege ###
* Kérdéseket lehet rögzíteni (Kérdés Admin)
* A kérdésekhez válaszokat lehet kapcsolni, köztük megjelölni, hogy melyik helyes (Válasz Admin)
* A program a kérdéseket és a válaszokat a felhasználó neve alapján kérdőívekbe rendezi
* A kérdőívekt kiértékeli (összeszámolja a helyes válaszokat, toplistát készít)
* Egy felhasználó egy kérdést csak egyszer válaszolhat meg, a kérdéseket random sorrendben kapja
* Egyes funkciók REST API-n keresztül is elérhetőek

### REST végpontok ###

#### Kérdések ####
* Összes kérdés lekérdezése - GET api/kerdesek
* Új kérdés - POST api/ujkerdes
* Kérdés visszadása ID alapján - GET api/kerdes/{id}
* Kérdés szöveg módosítás ID alapján - PUT api/kerdesmod/{id}

#### Válaszok ####
* Válasz lekérdezés ID alapján - GET api/valasz/{id}
* Válasz módosítása ID alapján - PUT api/valaszmod/{id}

#### Eredmény ####
* Összes eredmény lekérdezése - GET api/eredmenyek
* Eredmény lekérdezése ID alapján - GET api/eredmeny/{id}
* Eredmény válasz módosítása ID alapján - PUT api/eredmenymod/{id}
* Eredmény törlése - DELETE api/eredmenytorol/{id}

#### Kerdőiv ####
* Összes kérdőív lekérdezése - GET api/kerdoivek
* Kérdőív visszaadása id alapján - GET api/kerdoiv/{id}

### Telepítés ###

POM alapján IntelliJ-be projektént betöltve lefordul.

### Ismert hibák ###

* [JAVÍTVA] 4-5 kérdés megválaszolása után exceptiont dob
